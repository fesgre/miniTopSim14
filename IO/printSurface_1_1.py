﻿"""
Defines key press events for ploting funktions.

Author: 
        Bernhard Kößl
        (Please add your name if you modify this file)
"""

import matplotlib.pyplot as plt
from IO.readsurface import readsurface as  readsrf
import os
import IO.parameters as par

ax = plt.subplot(111)
global srfFile
srfFile = None

def getAbsFilePath():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    filename = os.path.join(dir_path, srfFile)    
    return filename

def calcNumberOfSurfaces():
    """calc number of surfaces ind .srf file"""   
    with open (getAbsFilePath(), "r") as file:
        data=file.read().replace('\n', '')
    return data.count("surface")

def event_handler(event):
    """
    key-press events
    
    '[ ]'  : nächster Plot
    '[0-9]': jede 2^n-te Oberfläche anzeigen
    '0'    : letzte Oberfläche anzeigen
    'r'    : erste Oberfläche anzeigen
    'a'    : Aspektverhältnis zwischen 1:1(equal) und automatisch(auto) hin- und herschalten
    'c'    : frühere Oberflächen beim Darstellen neuer löschen oder nicht
    'p'    : Plot in png-File schreiben
    'b'    : Darstellungsgrenzen fix oder angepasst
    'q'    : Anwendung verlassen
    """
    
    global c
    global clearFig
    global axis
    global aspect    
    x_Vals = list()
    y_Vals = list()
    srfNumber = 0
    
    absSurfNum = calcNumberOfSurfaces()
        
    while readsrf(srfNumber)[1:] != None: 
        x_Vals.append(readsrf(srfNumber)[:1])
        y_Vals.append(readsrf(srfNumber)[1:] )
        srfNumber = srfNumber + 1
        if srfNumber >= absSurfNum:
            break
    
    if event is None:
        c = 0
        clearFig = True
        axis = False 
        aspect = False                   

    else:
        if event.key in [' ']: 
            c += 1
            
        elif event.key in ['1','2','3','4','5','6','7','8','9']:
            a = int(event.key)
            c = c + 2 ** a
                        
        elif event.key in ['0']:
            c = absSurfNum - 1
            
        elif event.key in ['r']:
            c = 0
            
        elif event.key in ['a']:
            aspect = not aspect
            
        elif event.key in ['c']:
            clearFig = not clearFig
            
        elif event.key in ['p']:
            filename = par.INITIAL_SURFACE_FILE
            filename = filename.replace('.srf','.png')
            plt.savefig(filename)
            
        elif event.key in ['b']:
            axis = not axis
            
        elif event.key in ['q']:
            plt.close()
            
        else:
            print ('Key >%s< is not defined' % (event.key))
            return
            
    if  c >= len(x_Vals):
        plt.close()
        return
        
    if aspect:
        ax.set_aspect('equal')
    else:
        ax.set_aspect('auto')
        
    if clearFig:
        ax.clear()
    
    if axis:
        plt.axis([par.FUN_MIN, par.FUN_MAX, -10, 60])
        
    ax.plot(x_Vals[c], y_Vals[c], 'ro')
    ax.set_title("surface %i" % (c+1))
    plt.xlabel("[nm]")
    plt.ylabel("[nm]")
    plt.draw()

def doPlot():
    event_handler(None)
    plt.connect('key_press_event', event_handler)
    plt.show()

