"""
Calculates the surface velocities

Author: Gerhard Hobler
        Valentin Kleibel
        Florian Patocka
        (Please add your name if you modify this file)
"""

import IO.parameters as par
from physics import fluxes
from physics import sputtering
import numpy as np

def velocities(surface):
    """Calculate the velocity/velocities."""
    if par.ETCHING:
        return par.ETCH_RATE
    else:
        return (fluxes.sputFlux(surface)-fluxes.redepFlux(surface,fluxes.sputFlux(surface)))/par.DENSITY
    

