"""
Test the Surface class.

Author: Gerhard Hobler
        Daniel A. Maierhofer
        Florian Wagner (all parts deleted by the same person)
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())

import IO.parameters as par

from surface.surface import Surface

def test_surface():    
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'test_surface.cfg'))
    
    import matplotlib.pyplot as plt

    plt.title('Isotropic Etching')
    
    surface = Surface()
    x, y = surface.vertices
#    print(surface.directions)
    plt.plot(x, y, 'b-', label='initial')
    plt.plot(x, y, 'bo')
    plt.xlabel('horizontal [nm]')
    plt.ylabel('vertical [nm]')    

    surface.advance(10.)    
    x, y = surface.vertices
    plt.plot(x, y, 'g-', label='1 big timestep')
    plt.plot(x, y, 'go')
    
    surface = Surface()
    for i in range(10):
        surface.advance(1.)
    x, y = surface.vertices
    plt.plot(x, y, 'r-', label='10 timesteps')
    plt.plot(x, y, 'ro')
        
    plt.legend(loc='lower right')
    plt.show()

if __name__ == '__main__':
    test_surface()

