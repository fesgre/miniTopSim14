"""
Test the adaptive grid functionality of the surface class.

Author: Matthias Kirschner
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())

from minitopsim import main
import IO.plot as plot


def test_adaptive_grid():
    '''Test adaptive grid'''
    dir_path = os.path.dirname(os.path.abspath(__file__))
    
    plot.init('Isotropic Etching with Adaptive Grid')
        
    for time, surface in main(os.path.join(dir_path, 'test_adaptive_grid.cfg')):
        if time == 0:
            plot.add_surface(surface, 'initial surface')

    plot.add_surface(surface, 'time = ' + str(time))
    plot.show()

if __name__ == '__main__':
    test_adaptive_grid()

