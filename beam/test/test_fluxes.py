"""
test fluxes function in beam

Author: Valentin Kleibel
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())

import IO.parameters as par

from beam.beam import Beam
from numbers import Number

def test_fluxes():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'beam_flux.cfg'))

    assert isinstance(Beam.fluxes(), Number)
    
if __name__ == '__main__':
    test_fluxes()