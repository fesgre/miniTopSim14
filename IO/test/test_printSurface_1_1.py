﻿"""
Define function to test key press events for ploting funktions.

Author: 
        Bernhard Kößl
        (Please add your name if you modify this file)
"""

import numpy as np
import matplotlib.pyplot as plt

x_Vals = list()
y_Vals = list()
maxRange = 20

for i in range(0, maxRange): #sorge für testdaten
    x_Vals.append(np.random.rand(20))
    y_Vals.append(np.random.rand(20))

print(x_Vals, y_Vals)
ax = plt.subplot(111)

def event_handler(event):
    """ on key-press events show next plot """
    global c
    global clearFig
    global axis
    global aspect
    
    if event is None:
        c = 0
        clearFig = True
        axis = False 
        aspect = False                   

    else:
        if event.key in [' ']: 
            c += 1
            
        elif event.key in ['1','2','3','4','5','6','7','8','9']:
            a = int(event.key)
            c = c + 2 ** a
                        
        elif event.key in ['0']:
            c = maxRange - 1
            
        elif event.key in ['r']:
            c = 0
            
        elif event.key in ['a']:
            aspect = not aspect
            
        elif event.key in ['c']:
            clearFig = not clearFig
            
        elif event.key in ['p']:
            filename = "test.srf"
            filename = filename.replace('.srf','.png')
            plt.savefig(filename)
            
        elif event.key in ['b']:
            axis = not axis
            
        elif event.key in ['q']:
            plt.close()
            
        else:
            print ('Key >%s< is not defined' % (event.key))
            return
            
    if  c >= len(x_Vals):
        plt.close()
        return
        
    if aspect:
        ax.set_aspect('equal')
    else:
        ax.set_aspect('auto')
        
    if clearFig:
        ax.clear()
    
    if axis:
        plt.axis([-100, 100, -10, 60])
        
    ax.plot(x_Vals[c], y_Vals[c], 'ro')
    ax.set_title("surface %i" % (c+1))
    plt.xlabel("[nm]")
    plt.ylabel("[nm]")
    plt.draw()
    
event_handler(None) 
plt.connect('key_press_event', event_handler)
plt.show()


