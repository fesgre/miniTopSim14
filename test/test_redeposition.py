"""
Test for the deposition functionality
of the miniTopSim

Author: Florian Patocka
        (add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())

import IO.plot as plot
import IO.parameters as par
from minitopsim import main

def test_redeposition():
    """Test redeposition."""
    plot.init('Sputtering - Redeposition')
    
    dir_path = os.path.dirname(os.path.abspath(__file__))
    for time, surface in main(os.path.join(dir_path, 'test_redeposition1.cfg')):
        if time == 0:
            plot.add_surface(surface, 'initial surface')
            
    plot.add_surface(surface, 'w/o redeposition')
    for time, surface in main(os.path.join(dir_path, 'test_redeposition2.cfg')):
        continue
    plot.add_surface(surface, 'w/ redeposition')
   
  
    plot.show()
    
if __name__ == '__main__':
    test_redeposition()
