import sys
from PySide import QtGui
from IO.parameters import Parameters
from collections import OrderedDict
from functools import partial

__author__ = 'Felicia'



class Interface(QtGui.QMainWindow):
    
    def __init__(self):
        super(Interface, self).__init__()
        self.par = Parameters()
        self.init_ui()
        self.fname = ""
        self.saved = True

    def closeEvent(self, event):
        if not self.saved:
            reply = QtGui.QMessageBox.question(self, 'Parameters not saved',
                    "The parameters have not been saved.\nDo you want to save?", QtGui.QMessageBox.Ok |
                    QtGui.QMessageBox.Cancel, QtGui.QMessageBox.Cancel)

            if reply == QtGui.QMessageBox.Ok:
                self.save_config()


    def init_ui(self):
        #setup menu bar
        exitAction = QtGui.QAction('&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        loadAction = QtGui.QAction('&Load', self)
        loadAction.setShortcut('Ctrl+L')
        loadAction.setStatusTip('Load config file')
        loadAction.triggered.connect(self.load_config)

        saveAction = QtGui.QAction('&Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Save config file')
        saveAction.triggered.connect(self.save_config)

        saveasAction = QtGui.QAction('&Save as', self)
        saveasAction.setShortcut('Ctrl+Alt+S')
        saveasAction.setStatusTip('Save as config file')
        saveasAction.triggered.connect(self.save_as_config)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(loadAction)
        fileMenu.addAction(saveAction)
        fileMenu.addAction(saveasAction)
        fileMenu.addSeparator()
        fileMenu.addAction(exitAction)      
            
        self.load_interface()

        self.setGeometry(300, 300, 500, 150)
        self.setWindowTitle('Parameters GUI')
        self.show()
        

    def load_interface(self):
        widget = QtGui.QTabWidget()
        self.section_dict = OrderedDict()

        for section in self.par:
            tab = QtGui.QWidget()
            gridElement = QtGui.QFormLayout()
            elem_dict = OrderedDict()
            indexElem = 0
            for key, val in self.par[section].items():
                if isinstance(val, bool):
                    elem = QtGui.QCheckBox(self)
                    elem.setChecked(val)
                    elem.stateChanged.connect(partial(self.onChanged, section, key))
                else:
                    elem = QtGui.QLineEdit(str(val), self)
                    elem.setFixedWidth(200)
                    elem.textChanged.connect(partial(self.onChanged, section, key))
                    
                #add label
                if self.par.is_default(section, key):
                    label_text = key
                else:
                    label_text = """<b>%s</b>"""%key
                lbl = QtGui.QLabel(label_text, self)
                lbl.setFixedWidth(200)
                #add element to grid
                elem.setToolTip(str(self.par.get_description(section, key)))
                gridElement.addRow(lbl,elem)
                elem_dict[key] = (lbl, elem)
                indexElem += 1
            tab.setLayout(gridElement)
            widget.addTab(tab,section)
            self.section_dict[section] = elem_dict

        self.setCentralWidget(widget)

    def onChanged(self, section, key, event):
        self.saved = False
        val = self.get_value(section, key)
        val = self.par.set_value(section, key, val)
        #write it back again when it has changed because it was invalid
        self.set_value(section, key, val)
        

    def store_all(self):
        for section in self.section_dict:
            for key in self.section_dict[section]:
                val = self.get_value(section, key)
                self.par.set_value(section, key, val)
                self.set_value(section, key, val)
        self.saved = True

    def get_value(self, section, key):
        elem = self.section_dict[section][key][1]
        if isinstance(elem, QtGui.QLineEdit):
            val = elem.text()
        elif isinstance(elem, QtGui.QCheckBox):
            val = bool(elem.isChecked())
        else:
            val = None
        #get value from parameter db and compare type
        db_val = self.par.get_value(section, key)
        if isinstance(db_val, bool):
            val = bool(val)
        elif isinstance(db_val, float):
            val = self._parse_float(val, db_val)
        elif isinstance(db_val, int):
            val = self._parse_float(val, db_val)
        elif isinstance(db_val, str):
            val = str(val)
        return val

    def _parse_float(self, val, alt):
        try:
            return float(val)
        except Exception:
            return alt

    def set_value(self, section, key, val):
        #get value from parameter db and compare type
        lbl, elem = self.section_dict[section][key]
        if isinstance(elem, QtGui.QLineEdit):
            elem.setText(str(val))
        elif isinstance(elem, QtGui.QCheckBox):
            elem.setChecked(val)
        else:
            raise Exception("Input box has the wrong type (nor QLineEdit, QCheckBox)")
        #update label
        if self.par.is_default(section, key):
            label_text = key
        else:
            label_text = """<b>%s</b>"""%key
        lbl.setText(label_text)


    def load_config(self):
        """This function will load a config and update the gui"""
        fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open config', filter="Config file (*.cfg)")
        if fname:
            self.fname = fname
            self.par.load_config(fname)
            self.load_interface()

    def save_config(self):
        if self.fname:
            self.par.write(self.fname)
        else:
            self.save_as_config()

    def save_as_config(self):
        fname, _ = QtGui.QFileDialog.getSaveFileName(self, 'Save config', filter="Config file (*.cfg)")
        if fname:
            self.fname = fname
            self.par.write(self.fname)




if __name__ == '__main__':
    """app = QtGui.QApplication.instance()"""
    """if app == None:"""
    """app = QtGui.QApplication(sys.argv)
    ex = Interface()
    sys.exit(app.exec_())"""
    
    app=QtGui.QApplication.instance() # checks if QApplication already exists 
    if not app: # create QApplication if it doesnt exist 
        app = QtGui.QApplication(sys.argv)
    ex = Interface()
    sys.exit(app.exec_())
"""    sys.exit(app.exec_())"""