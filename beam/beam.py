"""
Defines the Beam class.
Includes Functions for computing beam flux

Author: Gerhard Hobler
		Johannes Huemer 1226630
        Florian Patocka
        (Please add your name if you modify this file)
"""

import IO.parameters as par
import numpy as np
from scipy.special import erf


class Beam:

    def fluxes():
        """Beam flux from beam current density, assumed to be constant."""
        return par.BEAM_CURRENT_DENSITY / 1.6022e-19
		    

    def beam_flux(x):
        """Beam flux depending on BEAM_TYPE, data from database/.cfg file"""  
        
        if((par.BEAM_TYPE != 'constant' and (par.FWHM == 0 or par.SCAN_WIDTH == 0)) or\
        (par.BEAM_TYPE == 'error function' and par.ERF_BEAM_WIDTH == 0)):
            print('Check parameters!(Division by zero error)')
        else:
           sigma = par.FWHM/np.sqrt(8*np.log(2))
           Fbeam = 0
			
           if par.BEAM_TYPE == 'constant':
               Fbeam = par.BEAM_CURRENT_DENSITY/ 1.6022e-19 
           elif par.BEAM_TYPE == 'Gaussian' :
               Fbeam = (par.BEAM_CURRENT/(1.6022e-19*np.sqrt(2*np.pi)*sigma*par.SCAN_WIDTH))*\
               np.exp(-((x-par.BEAM_CENTER)**2)/(2*(sigma**2)))
           elif par.BEAM_TYPE == 'error function':        	
               x1 = par.BEAM_CENTER - par.ERF_BEAM_WIDTH/2
               x2 = par.BEAM_CENTER + par.ERF_BEAM_WIDTH/2    
               
               Fbeam = (par.BEAM_CURRENT/(1.6022e-19*2*par.SCAN_WIDTH*par.ERF_BEAM_WIDTH))*\
               (erf(-(x-x2)/(sigma*np.sqrt(2)))-erf(-(x-x1)/(sigma*np.sqrt(2))))
           return Fbeam
        return 0
    
    
    
        

        
        