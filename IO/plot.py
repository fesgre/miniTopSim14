"""
Define functions for plotting a surface.

Author: Gerhard Hobler
        (Please add your name if you modify this file)
"""

import matplotlib.pyplot as plt

def init(title=None):
    if title is not None:
        plt.title(title)
    plt.xlabel('horizontal [nm]')
    plt.ylabel('vertical [nm]')    

def add_surface(surface, label):
    x, y = surface.vertices
    lines = plt.plot(x, y, '-', label=label)
    color = lines[0].get_color()
    plt.plot(x, y, 'o', color=color)
    
def show():
    plt.legend(loc='lower right')
    plt.show()

