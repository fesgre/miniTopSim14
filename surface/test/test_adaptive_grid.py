"""
Test the adaptive grid functionality of the surface class.

Author: Matthias Kirschner
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())

import IO.parameters as par

from surface.surface import Surface
import numpy as np

def test_surface():    
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'test_adaptive_grid.cfg'))
        
    surface = Surface()
    
    x=np.array([0,2,2.2,2.5,3,4,5,6])
    y=np.array([0,0,0,0,0,5,5,5])
    
    surface.vertices = x,y

    surface.advance(1.)
        
    x, y = surface.vertices
    
    x = np.round(x, decimals = 8)
    y = np.round(y, decimals = 8)
    
    x_check = np.array([0., 1., 2., 2.5, 3., 3.16666667, 3.306769, 3.44410768,\
               3.57597358, 3.69976551, 3.81304156, 3.91356726, 3.99935965, \
               4.06872638, 4.12029913, 4.15306059, 4.31972726, 4.48639392, \
               4.65306059, 4.84712709, 5, 6.])
    y_check = np.array([-1., -1., -1., -1., -1., -1., -0.99013703, -0.96074267,\
               -0.91239676, -0.84605297, -0.76301998, -0.66493571       \
               -0.55373495, -0.43161125, -0.30097361, -0.16439899,      \
               0.83560101, 1.83560101, 2.83560101, 4., 4., 4.])
                   
    assert np.array_equal(x,x_check) or np.array_equal(y,y_check)
    
if __name__ == '__main__':
    test_surface()


