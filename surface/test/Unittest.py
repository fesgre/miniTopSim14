# -*- coding: utf-8 -*-
"""
Created on Thu Nov 13 14:43:38 2014

@author: Alexander Bulyha
"""

from surface.surface import Surface
x1=Surface()
x2=Surface()
print("(aus mathematics.surface analytic_surface)\nselbe funktion: " + str(x1.surface_distance(x2)))
x2.advance(1)
print("ein surface um 1 timestep advanced: " + str(x1.surface_distance(x2)))
x2.advance(9)
print("ein surface um 10(gesamt) 'advanced': " + str(x1.surface_distance(x2)))
x1.advance(9)
print("das andere surface um 9 'advanced': " + str(x1.surface_distance(x2)))
x1.advance(1)
print("beide um 10: " + str(x1.surface_distance(x2)))
