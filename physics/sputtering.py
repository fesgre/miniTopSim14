"""
Calculates sputter yield
input parameter angle theta (number or array-like in rad)
output: number or numpy array

Author: Valentin Kleibel
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import IO.parameters as par
import numpy as np
from scipy.interpolate import PiecewisePolynomial
from os.path import isdir
from os.path import join

def syield(theta):
    global yieldinterpol, syieldfile
    
    yieldinterpol = None
    syieldfile = par.SPUTTER_YIELD_FILE
    
    theta = np.asanyarray(theta, dtype=np.float)
    sputteryield = np.zeros_like(theta, dtype=np.float)
    # check for abs(theta) >= 90 --> no sputtering possible
    mask = np.less(np.abs(theta), np.pi/2)

    if par.SPUTTER_YIELD_MODEL is 'Table':
        if yieldinterpol is None or syieldfile is not par.SPUTTER_YIELD_FILE:
            infile = join('tables', par.SPUTTER_YIELD_FILE)
            if isdir(infile):
                infile = join(infile, 'syield.pp')
            try:
                x, y0, y1 = np.loadtxt(infile, unpack=True)
                y = np.swapaxes([y0,y1], 0, 1)
            except:
                print('file given does not exist or is formatted incorrect')
                assert 0
                return
            syieldfile = par.SPUTTER_YIELD_FILE
            yieldinterpol = PiecewisePolynomial(x, y, orders=3, direction = None)
        sputteryield[mask] = yieldinterpol(np.abs(np.degrees(theta[mask])))
    
    else:
        sputteryield[mask] = ( (par.SPUTTER_YIELD_0*(np.cos(theta[mask]))**(-1*par.SPUTTER_YIELD_F))
                         *np.exp(par.SPUTTER_YIELD_B*(1-1/np.cos(theta[mask]))) )
    return sputteryield
