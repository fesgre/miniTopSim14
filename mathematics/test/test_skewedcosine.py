import os,sys
sys.path.insert(0,os.getcwd())
import mathematics.skewed_cosine as sc
from math import pi

def test_SkewedCosine():
    theta=30*pi/180
    alpha0=42.92*pi/180
    alpha=45*pi/180
    n=2.74

    _file=os.path.join(os.path.dirname(__file__),
                   "test_skewedcos_bang_scp.pp")

    SC=sc.SkewedCosine(_file)

    v1=SC.SkewedCosine_1(alpha,alpha0,theta,2.74)
    v2=SC.SkewedCosine(alpha,theta)
    
    error1=v1/0.8623208
    error2=v2/0.8557504

    if(error1>1):
        error1=error1-1

    if(error2>1):
        error2=error2-1


    assert(error1<1e-6)
    assert(error2<1e-6)

if __name__ == '__main__':
    test_SkewedCosine()
