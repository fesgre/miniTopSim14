"""
Test the minitTopSim for parameters.

Author: Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())

import IO.parameters as par

def test_parameters():
    """Test miniTopSim parameter parsing."""

    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'test_parameters.cfg'))

if __name__ == '__main__':
    test_parameters()
