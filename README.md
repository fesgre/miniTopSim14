miniTopSim
==========

Student project of the seminar "Scientific Programming in Python", WS 2014/15, TU Vienna

In diesem README finden Sie folgende Informationen:

    1. Allgemeine Hinweise
    2. Hinweise zur Präsentation
    3. Spezifizierung der Eingabeparameter
    
    
    
1. Allgemeine Hinweise
----------------------

Lesen Sie sorgfältig die Folien "Working with GitHub" der letzten Vorlesung. Um Konflikte und Fehler zu vermeiden

- Ändern Sie keinen Code, wenn es nicht für Ihre Aufgabe nötig ist, insbesondere nicht in Paketen, in denen Sie eigentlich nichts zu tun haben.
- Pushen Sie nur getesteten, voll funktionsfähigen Code.
- Pushen Sie nicht erst im letzten Moment (in der Nacht vor dem Präsentationstermin).
- Sie sind verantwortlich dafür, dass Ihr Code funktioniert. Schreiben Sie daher genügend Tests. Tests, die einzelne Funktionen oder Klassen testen, geben Sie ins "test" Verzeichnis des betreffenden Pakets. Tests, die das main Programm von miniTopSim aufrufen, geben Sie in das "test" Unterverzeichnis des Projektverzeichnisses (miniTopSim).
- Löschen oder verändern Sie keine schon bestehenden Tests, außer sie werden obsolet.

Schreiben Sie möglichst übersichtlichen Code. Dies ist auch in Hinblick auf Ihren Kurzvortrag wichtig. Kommentare sollten hauptsächlich im doc-String am Beginn eines Moduls, einer Klasse bzw. einer Funktion stehen; im Code höchstens Einzeiler oder am Zeilenende.


2. Hinweise zur Präsentation
----------------------------

Sie sollen Ihre Arbeit in einem ca. 10-minütigen Vortrag präsentieren. Halten Sie den Vortrag in erster Linie für ihre Kollegen und berücksichtigen Sie deren Wissensstand. Ihr Vortrag soll die Aufgabenstellung darlegen, den Code präsentieren und die Ergebnisse der Tests und/oder Simulationen beschreiben. Sie können dazu mehrere Hilfsmittel verwenden, Powerpoint-Präsentation, Spyder, Editor. Sie können Ihren Laptop verwenden oder Ihre Files auf einem USB-Stick mitbringen. Unser Rechner hat Powerpoint und Spyder installiert. Kurze Rechnungen können Sie online laufen lassen, bei längeren wird es angebracht sein, die Ergebnisse vorzubereiten.


3. Spezifizierung der Eingabeparameter
--------------------------------------

Das Programm soll mit 

    python3 <path-to-project-directory>/minitopsim.py beispiel.cfg

aufgerufen werden können, wobei beispiel.cfg das Input-File bezeichnet.

Aus Tests des `<path-to-project-directory>/test` Verzeichnisses rufen Sie das Hauptprogramm mit main('beispiel.cfg') auf, wobei Sie 'beispiel.cfg' durch den Namen Ihres Input-Files (das Sie ebenfalls im test Verzeichnis ablegen) ersetzen.

beispiel.cfg ist wie folgt formatiert:

    [SectionName1]
    ParameterName1 = Wert1
    ParameterName2 = Wert2
    ...
    [SectionName2]
    ParameterNameN+1 = WertN+1
    ParameterNameN+2 = WertN+2
    ...

ParameterNameNNN ist durch den Namen des Parameters (Großbuchstaben!) zu ersetzen. Die Parameter sind in Gruppen („Sections”) eingeteilt. 

Welche Parameter in welchen Sections einzuführen sind, ist in den einzelnen Themen angegeben. "Einführen" heißt für Sie, dass Sie einen Eintrag in der Parameter-Datenbank (parameters.db Datei im IO Verzeichnis) vornehmen müssen. Die Parameter-Datenbank ist ein Textfile in folgendem Format:

    [SectionName1]
    ParameterName1 = (DefaultWert1, 'Bedingung1', '''Erklärung1''')
    ParameterName2 = (DefaultWert2, 'Bedingung2', '''Erklärung2''')
    ...
    [SectionName2]
    ParameterNameN+1 = (DefaultWertN+1, 'BedingungN+1', '''ErklärungN+1''')
    ParameterNameN+2 = (DefaultWertN+2, 'BedingungN+2', '''ErklärungN+2''')
    ...

Für jeden Parameter sind in einem Tupel der Defaultwert, eine Bedingung und eine Erklärung angegeben. Die ersten beiden Angaben können auch None sein. Die Bedingung ist ein gültiger boolscher Python-Ausdruck, in einem String gespeichert oder lediglich None, was als 'True' gewertet wird. Er kann denselben oder andere Parameternamen als Variablen enthalten. Die Erklärung ist ein String, der (bei Verwendung von Triple-Quotes) auch über mehrere Zeilen laufen kann. Wann immer ein neuer Parameter eingeführt wird, muss also auch ein Eintrag in der Parameter-Datenbank erfolgen.

Im Programm werden die Parameter über das parameters Modul (Datei parameters.py) des IO Pakets zur Verfügung gestellt. Dieses wird mit

    import IO.parameters as par

importiert, nach einem Aufruf von 

    par.read('beispiel.cfg')

stehen die Parameter dann unter den Namen par.ParameterNameI (ParameterNameI entsprechend ersetzen) zur Verfügung.

