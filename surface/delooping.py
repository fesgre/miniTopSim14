# -*- coding: utf-8 -*-
"""
Created on Tue Nov 25 18:49:36 2014

Defines methodes for delooping surfaces

Author: Florian Wagner
        (Please add your name if you modify this file)
"""

import numpy as np

import IO.parameters as par

"""check if input parameters are correct"""
def checkinputs(invar):
    if isinstance(invar, np.ndarray) is True:
        if len(invar) is 2:
            pass
        else:
            raise ValueError("Input parameter has wrong size")
    else:
        raise TypeError("At least one of the first four input parameter ist not a numpy array")
        
def calculate_intersection(points, cor_seg1, cor_seg2):
    d_p1 = points[1] - points[0]
    d_p2 = points[3] - points[2]
    
    """solve equation like Ax = b"""
    a = np.transpose(np.array([d_p1, -d_p2]))
    b = np.array(points[2] - points[0])

    """check if not singular"""
    if np.linalg.matrix_rank(a) == 2:
        x = np.linalg.solve(a, b)
        
        t = x[0]
        s = x[1]
        
        if cor_seg1 is True:
            t = 1 - t
        if cor_seg2 is True:
            s = 1 - s
        if 0 <= t < 1 and 0 <= s < 1:
            """return intersection point"""
            iseg_p = points[0] + (points[1] - points[0])*t
            return iseg_p
        else:
            return None
    else:
        return None
    

"""expects 4 point (2 of each section) as numpy.arrays on length 2"""
def have_instersection(points):
    
    for invar in points:
        checkinputs(invar)
    
    seccount = 0        
    
    iseg_p = np.array([0,0])

    iseg_p_t = calculate_intersection(points, False, False)
    if iseg_p_t is not None:
        seccount += 1
        iseg_p = iseg_p_t


    count_ref = 1        
    
    """if doublecheck true, switch start and end point of the sections and try again"""
    if par.LOOP_DOUBLECHECK is True:
        count_ref = 3

        points_new = [points[1],points[0],points[2],points[3]]
        iseg_p_t = calculate_intersection(points_new, True, False)
        if iseg_p_t is not None:
            seccount += 1
            iseg_p = iseg_p_t
        
        points_new = [points[0],points[1],points[3],points[2]]
        iseg_p_t = calculate_intersection(points_new, False, True)
        if iseg_p_t is not None:
            seccount += 1
            iseg_p = iseg_p_t
        
        points_new = [points[1],points[0],points[3],points[2]]
        iseg_p_t = calculate_intersection(points_new, True, True)
        if iseg_p_t is not None:
            seccount += 1
            iseg_p = iseg_p_t
    
    """at least 3 (or one) check must be true"""
    if seccount >= count_ref:
        return True, iseg_p
    else:
        return False, None

def deloop_surface(surface):
    x,y = surface.vertices
    i = j = -1
    while i < (len(x) - 2):
        i += 1
        j = i
        while j < (len(x) - 2):
            j += 1
            if j == (i - 1) or j == i or j == (i + 1):
                """sorrounding sections cannot intersect"""
                pass
            else:
                seg1_p1 = np.array([x[i], y[i]])
                seg1_p2 = np.array([x[i+1], y[i+1]])
                seg2_p1 = np.array([x[j], y[j]])
                seg2_p2 = np.array([x[j+1], y[j+1]])
                
                points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
                
                result, iseg_p = have_instersection(points)
                if result is True:
                    
                    x = np.concatenate([x[:i], [iseg_p[0]], x[j+1:]])
                    y = np.concatenate([y[:i], [iseg_p[1]], y[j+1:]])
                    i = j+1
                    
    surface.vertices = np.array((x, y))