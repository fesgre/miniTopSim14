"""
test beam module, some tests of functionality 
parameters from database/beam_flux.cfg

Author: Johannes Huemer 1226630
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())
from beam.beam import Beam
import IO.parameters as par
from scipy.special import erf
import numpy as np


def beam_flux_with_input(beamType,x,fhwm,currentDensity,current,scanWidth,center,erfBeamWidth):
        """Beam flux depending on BEAM_TYPE, ONLY FOR TESTING!"""  
        if(par.FWHM ==0 or par.SCAN_WIDTH == 0 or par.ERF_BEAM_WIDTH == 0):
            print('Check parameters!(Division by zero error)')
        else:
            sigma = fhwm/np.sqrt(8*np.log(2))
            
            if beamType =='constant':
                Fbeam = currentDensity/ 1.6022e-19 
            elif beamType == 'Gaussian':
                Fbeam = (current/(1.6022e-19*np.sqrt(2*np.pi)*sigma*scanWidth))*np.exp(-((x-center)**2)/(2*(sigma**2)))
            elif beamType == 'error function':        
                x1 = center - erfBeamWidth/2
                x2 = center + erfBeamWidth/2        
                Fbeam = (current/(1.6022e-19*2*scanWidth*erfBeamWidth))* \
                        (erf(-(x-x2)/(sigma*np.sqrt(2)))-erf(-(x-x1)/(sigma*np.sqrt(2))))
            return Fbeam
        return 0
		
def beam_constant():
    """Test function for BEAM_TYPE = constant"""
    
    Type = 'constant'
    Fbeam1=beam_flux_with_input(Type,par.BEAM_CENTER-1,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)
    Fbeam2=beam_flux_with_input(Type,par.BEAM_CENTER+1,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)  
    FbeamCenter=beam_flux_with_input(Type,par.BEAM_CENTER,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)
    
    
    assert (FbeamCenter == Fbeam1 and FbeamCenter == Fbeam2)
    
def beam_Gaussian():
    """Test function for BEAM_TYPE = Gaussian"""
    
    Type = 'Gaussian'
    Fbeam1=beam_flux_with_input(Type,par.BEAM_CENTER-1,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)
    Fbeam2=beam_flux_with_input(Type,par.BEAM_CENTER+1,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)  
    FbeamCenter=beam_flux_with_input(Type,par.BEAM_CENTER,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)
    FbeamCurrentZero =beam_flux_with_input(Type,par.BEAM_CENTER,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                0,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)

    assert (((beam_flux_with_input(Type,0,1,0,50,5,0,0)) / 5.863420788e19)  <= 1.00001)
    assert FbeamCurrentZero == 0  
    assert (FbeamCenter >= Fbeam1 and FbeamCenter >= Fbeam2)              
        
def beam_erFunction(): 
    """Test function for BEAM_TYPE = error function"""
        
    Type = 'error function'
    Fbeam1=beam_flux_with_input(Type,par.BEAM_CENTER-1,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)
    Fbeam2=beam_flux_with_input(Type,par.BEAM_CENTER+1,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)  
    FbeamCenter=beam_flux_with_input(Type,par.BEAM_CENTER,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                par.BEAM_CURRENT,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)
    FbeamCurrentZero =beam_flux_with_input(Type,par.BEAM_CENTER,par.FWHM,par.BEAM_CURRENT_DENSITY,\
                                0,par.SCAN_WIDTH,par.BEAM_CENTER,par.ERF_BEAM_WIDTH)
    
    assert (((beam_flux_with_input(Type,0,1,0,50,5,0,10)) / 6.241418050e18)  <= 1.00001)    
    assert FbeamCurrentZero == 0
    assert (FbeamCenter >= Fbeam1 and FbeamCenter >= Fbeam2)
        
def test_beam():    
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'beam_flux.cfg'))
    beam_constant()
    beam_Gaussian()
    beam_erFunction()
    print('Beam flux with Type = {} at x = {} is {}'.format(par.BEAM_TYPE,par.BEAM_POSITION,\
                                                            Beam.beam_flux(par.BEAM_POSITION)))
    print('Beam tests passed!')  


    
