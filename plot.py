# -*- coding: utf-8 -*-
"""
Created on Sun Nov 23 14:29:35 2014

@author: Konsti
         Bernhard Kößl
         (Please add your name if you modify this file)
"""
import sys
import os
import matplotlib.pyplot as plt

x_Vals = list()
y_Vals = list()

def getAbsFilePath():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    subpath = sys.argv[1]    
    filename = os.path.join(dir_path, subpath)      
    return filename
    
def calcNumberOfSurfaces():
    """calc number of surfaces ind .srf file"""
    
    with open (getAbsFilePath(), "r") as file:
        data=file.read().replace('\n', '')
    return data.count("surface")
    
def readsurface( index ):
    """read a surface from a file and return 2 arrays for x and y values"""
    
    x = list()
    y = list()
    
    i = 0    
    f = open(getAbsFilePath())
        
    for line in f:
        if line.find('surface:') != -1:
            i+=1
    f.seek(0,0)
    
    if index == None:
        index = i
    elif index < 0:
        if abs(index) > i:
            return None, None
        index = i + index + 1
    elif index > i:
        return None, None
    else:
        index += 1
    
    i = 0
    for line in f:
        if line.find('surface:') != -1:
            i+=1
        if index == i:
            values = line.split('\t')         
            try:
                x.append(float(values[0]))
                y.append(float(values[1]))
            except ValueError:
                pass
        if i > index:
            break
    f.close()
    return x,y

def event_handler(event):
    """ on key-press events show next plot """
    global c
    global clearFig
    global axis
    global aspect
    
    if event is None:
        c = 0 
        clearFig = True
        axis = False 
        aspect = False
                  
    else:
        if event.key in [' ']: 
            c += 1
            
        elif event.key in ['1','2','3','4','5','6','7','8','9']:
            a = int(event.key)
            c = c + 2 ** a
                        
        elif event.key in ['0']:
            c = absSurfNum - 1
            
        elif event.key in ['r']:
            c = 0
            
        elif event.key in ['a']:
            aspect = not aspect
            
        elif event.key in ['c']:
            clearFig = not clearFig
            
        elif event.key in ['p']:
            filename = sys.argv[1]
            x = filename.rfind("\\") + 1
            filename = filename[x:]
            filename = filename.replace('.srf','.png')
            plt.savefig(filename)
            
        elif event.key in ['b']:
            axis = not axis
            
        elif event.key in ['q']:
            plt.close()
            
        else:
            print ('Key >%s< is not defined' % (event.key))
            return
            
    if  c >= len(x_Vals):
        plt.close()
        return
        
    if aspect:
        ax.set_aspect('equal')
    else:
        ax.set_aspect('auto')
        
    if clearFig:
        ax.clear()
    
    if axis:
        plt.axis([-100, 100, -10, 60])
        
    ax.plot(x_Vals[c], y_Vals[c], 'ro')
    ax.set_title("surface %i" % (c+1))
    plt.xlabel("[nm]")
    plt.ylabel("[nm]")
    plt.draw()    

absSurfNum = calcNumberOfSurfaces()
number = 0
while  readsurface(number) != None: 
        x_Vals.append(readsurface(number)[:1])
        y_Vals.append(readsurface(number)[1:])
        number = number + 1
        if number >= absSurfNum:
            break
        
print("there are %i surfaces to print" % (absSurfNum))
ax = plt.subplot(111) 
event_handler(None)          
plt.connect('key_press_event', event_handler)
plt.show()