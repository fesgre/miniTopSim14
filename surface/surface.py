﻿"""
Defines the Surface class.
Author: Gerhard Hober
        Aliaksandr Bulyha
        Florian Wagner
        Matthias Kirschner
        (Please add your name if you modify this file)
"""

import numpy as np

from mathematics.surfaces import analytic_surface 
from physics.velocities import velocities
from math import sqrt
from IO.readsurface import readsurface
from IO.writesurface import writesurface
from surface.delooping import deloop_surface
import sys

from scipy import interpolate

from matplotlib import pyplot as plt

import IO.parameters as par

class Surface:
    """
    Surface class.
    """
    #vertices=np.array((np.zeros(1), np.zeros(1)))
    def __init__(self):
        """Initialize the surface."""
        x, y = analytic_surface()
        self.vertices = np.array((x, y))
        self.__directions()   # calculate self.directions

    def dij(self,b): 
        """Berechnet den rms Abstand der ni Knoten der Oberfläche i von der oberfläche j, erwartet zwei oberflächen"""                                    
        dkj=100000
        sumdkj=0
        xa, ya= self.vertices
        xb, yb= b.vertices
        xbvec= np.roll(xb,-1) - xb #xkoor der Segmentvektoren
        ybvec= np.roll(yb,-1) - yb
        for k in range (xa.size):
            for j in range (xb.size):
                    # xdist=s.vertices[j].x - self.vertices[k].x
                    #ydist=s.vertices[j].y - self.vertices[k].y
                    #distpp= sqrt(xdist**2 + ydist**2)
                distpp= sqrt((xb[j]-xa[k])**2 + (yb[j]-ya[k])**2)

                if distpp<dkj:
                    dkj=distpp
                    
            for j in range (xb.size -1):
                
                spx=xb[j] + xbvec[j]*((xa[k]-xb[j])*xbvec[j] +(ya[k]-yb[j])*ybvec[j])/(xbvec[j]**2+ybvec[j]**2) 
                spy=yb[j] + ybvec[j]*((xa[k]-xb[j])*xbvec[j] +(ya[k]-yb[j])*ybvec[j])/(xbvec[j]**2+ybvec[j]**2)
                #schnittpunkit der gerade mit der normalen durch k berechnet
                if spx>xb[j] and spx < xb[j+1]:
                    distpg= sqrt((spx-xa[k])**2 + (spy-ya[k])**2)
                    if distpg <dkj:
                        dkj=distpg
            sumdkj+=(dkj**2)
        return sqrt(1/(self.vertices.size+1) * sumdkj)
    
        
    def __directions(self):
        """Calculate the normal vectors and angles(rad) of normal vectors of the surface defined by vertices."""
        x, y = self.vertices
        dx, dy = x[1:] - x[:-1], y[1:] - y[:-1]
        # normal vectors of the segments
        nx, ny = dy, -dx
        n = np.hypot(nx, ny)
        nx /= n
        ny /= n
        # normal vectors at the inner vertices
        nvx = nx[1:] + nx[:-1]
        nvy = ny[1:] + ny[:-1]
        nv = np.hypot(nvx, nvy)
        nvx /= nv
        nvy /= nv
        # normal vectors at all vertices
        nvx = np.concatenate((nx[:1], nvx, nx[-1:]))
        nvy = np.concatenate((ny[:1], nvy, ny[-1:]))
        self.directions = np.array((nvx, nvy))
        self.angles = np.arctan((self.directions[0]/(-1*self.directions[1])))

    def advance(self, time_step):
        """Advance the vertices along the surface normals."""
        if par.ADAPTIVE_GRID is True:
            self.adapt_grid()
        
        self.vertices += time_step * velocities(self) * self.directions
        deloop_surface(self)
        self.__directions()   # calculate new self.directions
    
    def surface_distance(self, s):
        """Returns distance between two surfaces"""
        d12=self.dij(s)
        d21=s.dij(self)
        return sqrt(d12**2 + d21**2)
        
    def writeSurfaceToFile(self):
        x,y = self.vertices
        writesurface(x,y)
    
    def readSurfaceFromFile(self,index):
        x,y = readsurface( index )
        self.vertices = x,y

    def adapt_grid(self, method='linear'):
        """Adapts surface so that the distance of adjacent points is not too
        large or too small (as defined by MAX_SEGLEN) and angles between normal
        vectors of adjacent knots do not exceed MAX_ANGLE.
        
        method : str, optional
            Method of 
            Possible values are 'linear', 'quadratic'. 'cubic' """ 
        x, y = self.vertices
                        
        # Check if x-values are monotonically increasing
        if np.any( x[1:] - x[:-1] < 0):
            print('Error: X-Values are not monotonically increasing!')
            sys.exit()
        
        # Check if the segments are too short and remove knot if so
        max_angle = np.deg2rad(par.MAX_ANGLE)
        
        status = True
        while(status):
            dx = x[2:] - x[:-2]
            dy = y[2:] - y[:-2]
            double_seglen = np.hypot(dx, dy)
            
            remove_indices, = np.where(double_seglen < par.MAX_SEGLEN)
            
            if remove_indices.size == 0:
                break
            
            for remove_index in remove_indices:
                if abs(self.angles[remove_index+2]-self.angles[remove_index]) < max_angle:
                    x = np.delete(x, remove_index+1)
                    y = np.delete(y, remove_index+1)
                    self.vertices = np.array((x,y))
                    self.__directions()
                    break
            else:
                status = False
        
        plt.plot(x,y,'b+')
                    
            
            
        # Check if the segments are too long and insert knot if so
        dx = x[1:] - x[:-1]
        dy = y[1:] - y[:-1]
        seglen = np.hypot(dx, dy)
        
        long_seg_index, = np.where(seglen > par.MAX_SEGLEN)
                
        if long_seg_index.size > 0:
            split_value = seglen[long_seg_index] / par.MAX_SEGLEN
            split_value = np.ceil(split_value)

            f = interpolate.interp1d(x, y, method)
            
            index_insert = list()
            x_insert = list()
            
            for i in range(long_seg_index.size):
                index_local = long_seg_index[i]
                index_new = np.ones(split_value[i] - 1) * (index_local + 1)
                index_insert.append(index_new)
                
                x_new = np.linspace(x[index_local], x[index_local+1], 
                                num=split_value[i], endpoint=False)
                x_insert.append(x_new[1:])
        
            x_insert = np.hstack(x_insert)
            y_insert = f(x_insert)
            index_insert = np.hstack(index_insert)
        
            x = np.insert(x, index_insert, x_insert)
            y = np.insert(y, index_insert, y_insert)
                        
        self.vertices = np.array((x,y))
        self.__directions()
        
        

        # Check if angle between normal vectors of adjacent knots is too large
        # and insert additional knots (same coordinates, different normal vec)
        x,y = self.vertices
        nvx,nvy = self.directions
        
        # Calculate angles between segments
        dx = x[1:] - x[:-1]
        dy = y[1:] - y[:-1]
        seglen = np.hypot(dx, dy)

        seg_angles_diff = dx[:-1] * dx[1:] + dy[:-1] * dy[1:]
        seg_angles_diff /= (seglen[:-1] * seglen[1:])
        seg_angles_diff = np.clip(seg_angles_diff, -1., 1.)
        seg_angles_diff = np.arccos(seg_angles_diff)
                
        max_angle = np.deg2rad(par.MAX_ANGLE)
        
        large_angles, = np.where(seg_angles_diff > max_angle)
                
        if large_angles.size > 0:
            index_insert = list()
            x_insert = list()
            y_insert = list()
            nvx_insert = list()
            nvy_insert = list()
        
            for i in range(large_angles.size):
                index_local = large_angles[i]
                
                delta_angle_left = self.angles[index_local+1] - self.angles[index_local]
                local_split = np.ceil(abs(delta_angle_left / max_angle))
                local_split = int(local_split)
                
                rot_angle = delta_angle_left / local_split
                
                nvx_new = list()
                nvy_new = list()
                
                for k in range(local_split):
                    nvx_local = nvx[index_local] * np.cos(k*rot_angle)
                    nvx_local -= nvy[index_local] * np.sin(k*rot_angle)
                    
                    nvy_local = nvx[index_local] * np.sin(k*rot_angle)
                    nvy_local += nvy[index_local] * np.cos(k*rot_angle)
                
                    nvx_new.append(nvx_local)
                    nvy_new.append(nvy_local)
                
                index_new = np.array(np.ones(local_split) * index_local+1, dtype=int)
                
                index_insert.append(index_new)
                x_new = np.ones(local_split) * x[index_local+1]
                x_insert.append(x_new)
                y_new = np.ones(local_split) * y[index_local+1]
                y_insert.append(y_new)
                
                nvx_insert.append(nvx_new)
                nvy_insert.append(nvy_new)
                
                delta_angle_right = self.angles[index_local+2] - self.angles[index_local+1]
                
                local_split = np.ceil(abs(delta_angle_right / max_angle))
                local_split = int(local_split)
                
                rot_angle = delta_angle_right / local_split
                
                nvx_new = list()
                nvy_new = list()
                
                for k in range(1,local_split+1):
                    nvx_local = nvx[index_local+1] * np.cos(k*rot_angle)
                    nvx_local -= nvy[index_local+1] * np.sin(k*rot_angle)
                    
                    nvy_local = nvx[index_local+1] * np.sin(k*rot_angle)
                    nvy_local += nvy[index_local+1] * np.cos(k*rot_angle)
                
                    nvx_new.append(nvx_local)
                    nvy_new.append(nvy_local)
                
                index_new = np.array(np.ones(local_split) * index_local+2, dtype=int)
                
                index_insert.append(index_new)
                x_new = np.ones(local_split) * x[index_local+1]
                x_insert.append(x_new)
                y_new = np.ones(local_split) * y[index_local+1]
                y_insert.append(y_new)
                
                nvx_insert.append(nvx_new)
                nvy_insert.append(nvy_new)
                
            x_insert = np.hstack(x_insert)
            y_insert = np.hstack(y_insert)
            nvx_insert = np.hstack(nvx_insert)
            nvy_insert = np.hstack(nvy_insert)
            index_insert = np.hstack(index_insert)
        
            x = np.insert(x,index_insert,x_insert)
            y = np.insert(y,index_insert,y_insert)
            nvx = np.insert(nvx,index_insert,nvx_insert)
            nvy = np.insert(nvy,index_insert,nvy_insert)
        
            self.vertices = np.array((x,y))
            self.directions = np.array((nvx,nvy))
            self.angles = np.arctan((self.directions[0]/(-1*self.directions[1])))
