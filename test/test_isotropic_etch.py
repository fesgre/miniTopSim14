"""
Test the minitTopSim for isotropic etching.

Author: Gerhard Hobler
        Daniel A. Maierhofer
        Alexander Bulyha
        (Please add your name if you modify this file)
"""

import os, sys
import numpy as np
sys.path.insert(0, os.getcwd())

import IO.plot as plot
from minitopsim import main
from IO.readsurface import readsurface
from surface.surface import Surface

checkvalue=500 #lower this for asertioneror

def surf_dist_check (surf_dist,check_value):
    """compares surface distance to check_value, throws assertion Error"""
    assert (surf_dist < check_value)

def test_isotropic_etch_surfdist():
    """Test miniTopSim with an isotropic etch."""
    dir_path = os.path.dirname(os.path.abspath(__file__))
    s=Surface()
    for time, surface in main(os.path.join(dir_path, 'test_isotropic_etch.cfg')):
        x,y=readsurface(-1)
        s.vertices=np.array((x,y))
        sdist=surface.surface_distance(s)
        surf_dist_check(sdist,checkvalue)

def test_isotropic_etch():
    """Test miniTopSim with an isotropic etch."""
    
    plot.init('Isotropic Etching')

    dir_path = os.path.dirname(os.path.abspath(__file__))

    for time, surface in main(os.path.join(dir_path, 'test_isotropic_etch.cfg')):
        if time == 0:
            plot.add_surface(surface, 'initial surface')

    plot.add_surface(surface, 'time = ' + str(time))
    plot.show()

if __name__ == '__main__':
    test_isotropic_etch()
    test_isotropic_etch_surfdist()
