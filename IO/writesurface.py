"""
Define function to write a surface to a file.

Author: Stefan Durstberger
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import IO.parameters as par
import sys, os
from time import clock

"""Define static counter var"""  
def static_var(varname, value):
    def decorate(func):
        setattr(func, varname, value)
        return func
    return decorate
@static_var('counter', 1)

def writesurface(x,y):
    """write a surface to a file"""
    if par.SAFE_TIME_STEP == None:
        timestep = 1e30
    else:
        timestep = par.SAFE_TIME_STEP

    if clock() > writesurface.counter * timestep:
        writesurface.counter += 1
        
        """generate file name out of .cfg file: bsp.cfg --> bsp.srf"""
        if par.SAFE_FILE == '':
            try:
                filename = sys.argv[1]
            except IndexError:
                filename = 'SurfaceData.srf'
            
            for j in filename.split(os.path.sep):
                if j.find('.') != (-1):
                    k = j.split('.')
                    l = (k[0] + '.srf')
            filename = (os.path.join('IO', l))
        else:
            filename = (os.path.join('IO', par.SAFE_FILE))
        
        if par.SAFE_TIME_STEP == None:
            time = 'None'
        else:
            time = par.SAFE_TIME_STEP
            
        f = open(filename, 'a')
        f.write('surface: %s, %d, x-position, y-position' %(time, len(x)))
        f.write('\n')
        for i in range(len(x)):
            f.write('%d\t%f' %(x[i],y[i]))
            f.write('\n')
        f.close()