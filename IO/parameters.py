"""
Container for the input parameters.

Author: Gerhard Hobler
        Daniel A. Maierhofer
        Felicia Brame
        (Please add your name if you modify this file)
"""
from collections import OrderedDict
import os
import configparser
from collections.abc import MutableMapping


PARAMETERDB = 'IO/parameters.db'

class Parameters(MutableMapping):
    def __init__(self):
        #contains the 'is' values of parameters
        self._parameters = OrderedDict()
        #contains the tuple of parameter.db (10.0, VAL > 0.0, "Example")
        self._db = OrderedDict()
        self._dbparser = configparser.ConfigParser()
        self._config = configparser.ConfigParser()
        self.load_database()

    def load_database(self):
        self._dbparser.read(PARAMETERDB)
        self._update_parameters()

    def load_config(self, fname):
        self._config.read(fname)
        self._update_parameters()

    def _update_parameters(self):
        for section in self._dbparser.sections():
            params = OrderedDict()
            db = OrderedDict()
            for key, val in self._dbparser[section].items():
                val = eval(val)
                if len(val)<3:
                    raise Exception("Tuple for %s is too short"%key)
                if self._config.has_section(section) and key in self._config[section]:
                    is_val = eval(self._config[section][key])
                else:
                    is_val = val[0]
                params[key.upper()] = is_val
                db[key.upper()] = val
            self._parameters[section] = params
            self._db[section] = db

    def get_value(self, section, key):
        return self._parameters[section][key.upper()]

    def get_default(self, section, key):
        return self._db[section][key.upper()][0]

    def get_constrain(self, section, key):
        return self._db[section][key.upper()][1]

    def get_description(self, section, key):
        return self._db[section][key.upper()][2]

    def is_default(self, section, key):
        """returns true if the value is the same as the default"""
        return self.get_value(section, key) == self.get_default(section, key)

    def set_value(self, section, key, value):
        """setuzt meien Wert, wenn nicht gueltig wird alter Wert verwendet"""
        old = self._parameters[section][key.upper()]
        self._parameters[section][key.upper()] = value
        #if the new value is not valid use the old one
        if not self.is_valid(section, key):
            print("not valid")
            self._parameters[section][key.upper()] = old
            return old
        return value

    def is_valid(self, section, key):
        #load section into locals so we could use eval()
        locals().update(self._parameters[section])
        ret = eval(str(self.get_constrain(section, key)))
        return ret is None or ret

    def write(self, fname):
        """write config to file"""
        print("write file")
        self._config.clear()
        for section in self._parameters:
            for key, val in self._parameters[section].items():
                #save only values that are not default
                if not self.is_default(section, key):
                    if not section in self._config:
                        self._config[section] = OrderedDict()
                    self._config[section][key] = str(val)

        #create folder if is does not exist
        basedir = os.path.dirname(fname)
        if basedir and not os.path.exists(basedir):
            os.makedirs(basedir)

        with open(fname, 'w') as configfile:
            self._config.write(configfile)

    def __getitem__(self, key):
        return self._parameters[key]

    def __setitem__(self, key, value):
        self._parameters[key] = value

    def __delitem__(self, key):
        print("del item not possible")

    def __len__(self):
        return len(self._parameters)

    def __iter__(self):
        return iter(self._parameters)



def __parse(filename, dictvar, dictcond = None):
    """Parse database or config file into dictionaries."""
    if type(filename) is list:
        filename = filename[0]
    config = configparser.SafeConfigParser()
    config.read(filename)
    sections = config.sections()
    if len(sections) < 1:
        msg = filename + ' empty or could not be opened'
        raise Exception(msg)
    for section in sections:
        options = config.options(section)
        for option in options:
            entry = eval(config.get(section, option))
            if dictcond is None:
                dictvar[option.upper()] = entry
            else:
                dictvar[option.upper()] = entry[0]
                dictcond[option.upper()] = entry[1]
                if type(entry[2]) is not str:
                    msg = 'Type of description for entry ' + option + \
                    ' in ' + PARAMETERDB + ' is not str'
                    raise Exception(msg)

def __check_cfgvars(dictdb, dictcfg):
    """Compare types of database and config file entries."""
    for name, value in dictcfg.items():
        if not name in dictdb:
            msg = 'No entry for ' + name + ' in ' + PARAMETERDB
            raise Exception(msg)
        if type(dictdb[name]) is float and type(value) is int:
            value = float(value)
            dictcfg[name] = value
        if dictdb[name] is not None:
            if type(value) is not type(dictdb[name]):
                msg = 'Type of entry ' + name + ' is not same as in ' + PARAMETERDB
                raise Exception(msg)

def __check_conds(conds):
    """Check parameter conditions from database."""
    for cond in conds:
        if conds[cond] is not None:
            if not eval(str(conds[cond])):
                msg = cond + ': ' + conds[cond] + ' not fullfilled'
                raise Exception(msg)

def read(filename):
    """Read database and specified config file."""
    dbvars = {}
    dbconds = {}
    cfgvars = {}

    dir_path = os.path.dirname(os.path.abspath(__file__))

    __parse(os.path.join(dir_path, PARAMETERDB), dbvars, dbconds)
    __parse(filename, cfgvars)

    __check_cfgvars(dbvars, cfgvars)

    print (dbvars)
    globals().update(dbvars)
    globals().update(cfgvars)

    __check_conds(dbconds)

if __name__ == "__main__":
    config = Parameters()
    for key in config['Numerics']: print(key)

    par = Parameters()
    par.load_config("bsp.cfg")

    for key, val in par._parameters.items():
        print(key, val)

    print(par.get_constrain("Initial Conditions", "DELTA_X"))
    print("set value to -1, which violates DELTA_X > 0")
    par.set_value("Initial Conditions", "DELTA_X", -1)
    print("new value: ", par.get_value("Initial Conditions", "DELTA_X"))

    print("set value to 2.0, which is fine")
    par.set_value("Initial Conditions", "DELTA_X", 2.0)
    print("new value ", par.get_value("Initial Conditions", "DELTA_X"))

    print("set value to 0.0, which violates DELTA_X > 0")
    par.set_value("Initial Conditions", "DELTA_X", 0)
    print("new value ", par.get_value("Initial Conditions", "DELTA_X"))

    print("set value XMIN = -10, which is fine")
    par.set_value("Initial Conditions", "XMIN", -10)
    print("new value ", par.get_value("Initial Conditions", "XMIN"))

    print("set value XMIN = XMAX, which violates XMIN < XMAX")
    par.set_value("Initial Conditions", "XMIN", par.get_value("Initial Conditions", "XMAX"))
    print("new value (should be -10): ", par.get_value("Initial Conditions", "XMIN"))

    par.write("test/bsp.cfg")