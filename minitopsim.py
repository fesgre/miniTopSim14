"""
Main program for topography simulation.

Author: Gerhard Hobler
        Daniel A. Maierhofer
        Konstantin Zabransky
        (Please add your name if you modify this file)
"""

import sys
from time import clock
from surface.surface import Surface
import matplotlib.pyplot as plt
import IO.parameters as par
import IO.printSurface_1_1 as pltsrf

global srfNumber
x_Vals = list()
y_Vals = list() 

def main(*argv):
    """
    Main program.     
    
    Implemented as a generator which returns the time and the surface after each timestep.
    """
  
    par.read(argv[0])
    
    CPU_start_time = clock()

    # Initialize the surface
    initial_time = 0
    surface = Surface()
    yield initial_time, surface
    
    # Do the topography simulation
    for timestep, time in timesteps(initial_time):
        surface.advance(timestep)
        yield time, surface

    print('\nCPU time:', clock() - CPU_start_time)
    
    if par.DISPLAY_SURFACE is True:
        pltsrf.srfFile = par.INITIAL_SURFACE_FILE
        pltsrf.doPlot()
    
def timesteps(initial_time):
    """
    Get next time step.
    """
    
    time = initial_time

    while time < par.TOTAL_TIME - 1e-7 * par.TIME_STEP:
        if time < par.TOTAL_TIME - 2 * par.TIME_STEP:
            timestep = par.TIME_STEP
        elif time < par.TOTAL_TIME - par.TIME_STEP:
            timestep = 0.5 * (par.TOTAL_TIME - time)
        else:
            timestep = par.TOTAL_TIME - time
        time += timestep
        yield timestep, time

if __name__ == '__main__':
    for surface in main(sys.argv[1:]):
        pass
    

