"""
test sputtering in physics

Author: Valentin Kleibel
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())
from physics import sputtering
import IO.parameters as par

def test_syield():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'test_syield.cfg'))

    print('theta 0, 1, 2 ' + str(sputtering.syield([0, 1, 2])))

def test_syield_file():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'test_syield_file.cfg'))

    print(par.SPUTTER_YIELD_FILE)

    print('theta 0, 1, 2' + str(sputtering.syield([0, 1, 2])))
    par.SPUTTER_YIELD_FILE = os.path.join('gasi30', 'syield.pp')
    print(par.SPUTTER_YIELD_FILE)
    print('theta 0, 1, 2' + str(sputtering.syield([0, 1, 2])))

if __name__ == '__main__':
    test_syield()
    test_syield_file()
