"""
Define function to test read/write surface from/to file.

Author: Stefan Durstberger
        (Please add your name if you modify this file)
"""

import os,sys
sys.path.insert(0,os.getcwd())
import IO.parameters as par
import matplotlib.pyplot as plt
from surface.surface import Surface

def test_read_write_surface():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'test_read_write_surface.cfg'))
    surface = Surface()
    surface.__init__()
    surface.writeSurfaceToFile()
    surface.readSurfaceFromFile(-1)
    
    x,y = surface.vertices
    plt.title('Surface')
    plt.plot(x, y, 'r+-', label='f(x)')
    plt.legend()
    plt.show()
    
if __name__ == '__main__':
    test_read_write_surface()