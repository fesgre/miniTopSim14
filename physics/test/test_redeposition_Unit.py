"""
Unit-test for the redeposition
functionality.

Author: Florian Patocka
        (Please add your name if you modify this file)
"""

import numpy as np
import IO.parameters as par
from physics.fluxes import redepFlux

class TestSurface:
    """Surface class for testing"""
    def __init__(self, x, y, nvx, nvy):
        self.vertices = np.array((x,y))
        self.directions = np.array((nvx,nvy))
        
def test_redeposition1():
    
    par.REDEP_1 = True
    x = np.linspace(-10, 10, 21)
    y = np.zeros(21)
    nvx = np.zeros(21)
    nvy = -1*np.ones(21)

    surface = TestSurface(x,y,nvx,nvy)
    sputFlux = np.ones(21)
    
    assert(np.all(redepFlux(surface,sputFlux) == 0))
    """redeposited flux has to be zero"""

def test_redeposition2():
    
    par.REDEP_1 = True
    x = (-1,0,1)
    y = (1,0,1)
    nvx = (-1,0,+1)
    nvy = (0,-1,0)
    
    surface = TestSurface(x,y,nvx,nvy)
    sputFlux = np.ones(3)
    
    assert(np.isclose(redepFlux(surface,sputFlux)[1],0.5)) 