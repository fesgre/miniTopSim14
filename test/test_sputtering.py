"""
Test the minitTopSim for sputtering.

Author: Valentin Kleibel
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import os, sys
sys.path.insert(0, os.getcwd())

import IO.plot as plot
from minitopsim import main


def test_sputtering():
    """Test miniTopSim with sputtering."""
    
    plot.init('Sputtering')

    dir_path = os.path.dirname(os.path.abspath(__file__))

    for time, surface in main(os.path.join(dir_path, 'test_sputtering_Yamamura.cfg')):
        if time == 0:
            plot.add_surface(surface, 'initial surface')
    plot.add_surface(surface, 'Yamamura')
    for time, surface in main(os.path.join(dir_path, 'test_sputtering_table.cfg')):
        continue
    plot.add_surface(surface, 'table')
    plot.show()

if __name__ == '__main__':
    test_sputtering()
