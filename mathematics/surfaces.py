"""
    Define analytic surfaces for surface initialization.
    
    Author: Gerhard Hobler
    Stefan Durstberger
    (Please add your name if you modify this file)
    """

import numpy as np
import IO.parameters as par

def plane():
    """Define a plane function."""
    x = np.linspace(par.XMIN, par.XMAX, par.XMAX-par.XMIN+1)
    y = np.zeros(par.XMAX-par.XMIN+1)
    return x, y
    
def cosine():
    """Define a cosine function with flat extensions."""
    x = np.linspace(par.XMIN, par.XMAX, par.XMAX-par.XMIN+1)
    y = np.zeros(par.XMAX-par.XMIN+1)
    period = x[par.FUN_MAX-1] - x[par.FUN_MIN]
    y[par.FUN_MIN:par.FUN_MAX] = par.FUN_PEAK_TO_PEAK / 2 * (1 - np.cos(2*np.pi*(x[par.FUN_MIN:par.FUN_MAX] - x[par.FUN_MIN])/period))
    return x, y

def d_cosine():
    """Define a doublecosine function with flat extensions."""
    x = np.linspace(par.XMIN, par.XMAX, par.XMAX-par.XMIN+1)
    y = np.zeros(par.XMAX-par.XMIN+1)
    period = x[par.FUN_MAX-1] - x[par.FUN_MIN]
    y[par.FUN_MIN:par.FUN_MAX] = par.FUN_PEAK_TO_PEAK / 2 * (1 - np.cos(2*np.pi*(x[par.FUN_MIN:par.FUN_MAX] - x[par.FUN_MIN]) / period * 2))
    return x, y

def step():
    """Define a step function with flat extensions."""
    x = np.linspace(par.XMIN, par.XMAX, par.XMAX-par.XMIN+1)
    y = np.zeros(par.XMAX-par.XMIN+1)
    period = x[par.FUN_MAX] - x[par.FUN_MIN]
    y[par.FUN_MIN:par.FUN_MAX] = par.FUN_PEAK_TO_PEAK / period * (x[par.FUN_MIN:par.FUN_MAX] - x[par.FUN_MIN])
    y[par.FUN_MAX:] = par.FUN_PEAK_TO_PEAK * (x[par.FUN_MAX] - x[par.FUN_MIN]) / period
    return x, y

def v_shape():
    """Define a v-shaped function with flat extensions."""
    x = np.linspace(par.XMIN, par.XMAX, par.XMAX-par.XMIN+1)
    y = np.zeros(par.XMAX-par.XMIN+1)
    period = x[par.FUN_MAX] - x[par.FUN_MIN]
    y[par.FUN_MIN:(par.FUN_MAX-par.FUN_MIN)/2+par.FUN_MIN] = par.FUN_PEAK_TO_PEAK * 2 / period * (x[par.FUN_MIN:(par.FUN_MAX-par.FUN_MIN)/2+par.FUN_MIN] - x[par.FUN_MIN])
    y[(par.FUN_MAX-par.FUN_MIN)/2+par.FUN_MIN:par.FUN_MAX] = - par.FUN_PEAK_TO_PEAK * 2 / period * (x[(par.FUN_MAX-par.FUN_MIN)/2+par.FUN_MIN:par.FUN_MAX] - 2* x[(par.FUN_MAX-par.FUN_MIN)/2+par.FUN_MIN] + x[par.FUN_MIN])
    return x, y

def analytic_surface():
    """Generates a surface from the given shape out of the parameter file"""
    options = {'Plane' : plane, 
        'Cosine' : cosine,
        'DoubleCosine' : d_cosine,
        'Step' : step,
        'V-Shape' : v_shape,
    }
    
    try:
        x, y = options[ par.INITIAL_SURFACE_TYPE ]()
    except KeyError:
        x, y = cosine()
    
    return x, y