"""
Define function to test surface generation.

*******!!!!!!!!!!REQUIREMENTS!!!!!!!!!!!!!!***********
    - Parameter file miniTopSim/test/test_surfaces.cfg needed
    - Parameter SAFE_TIME_STEP must be very small (e.q. 0.001)
    
Author: Stefan Durstberger
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""


import os,sys
sys.path.insert(0,os.getcwd())
#import IO.writesurface as ws
import IO.parameters as par
import matplotlib.pyplot as plt
from surface.surface import Surface

def test_surfaces():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    par.read(os.path.join(dir_path, 'test_surfaces.cfg'))
    surface = Surface()
    surface.__init__
    x,y = surface.vertices
    
    if par.INITIAL_SURFACE_TYPE == 'Plane':
        for y_val in y[0:]:
            assert y_val == 0.0
    elif par.INITIAL_SURFACE_TYPE == 'Cosine':
        for y_val in y[0:par.FUN_MIN-1]:
            assert y_val == 0.0
        for y_val in y[par.FUN_MAX+1:]:
            assert y_val == 0.0
        assert abs( y[int((par.FUN_MIN + par.FUN_MAX) / 2)] ) > abs( 0.95 * par.FUN_PEAK_TO_PEAK)
    elif par.INITIAL_SURFACE_TYPE == 'DoubleCosine':
        for y_val in y[0:par.FUN_MIN]:
            assert y_val == 0.0
        for y_val in y[par.FUN_MAX:]:
            assert y_val == 0.0
        assert abs( y[int((3*par.FUN_MIN + par.FUN_MAX) / 4)] ) > abs( 0.95 * par.FUN_PEAK_TO_PEAK)
        assert abs( y[int((par.FUN_MIN + par.FUN_MAX) / 2)] ) < 0.5
        assert abs( y[int((par.FUN_MIN + 3 * par.FUN_MAX) / 4)] ) > abs( 0.95 * par.FUN_PEAK_TO_PEAK)
    elif par.INITIAL_SURFACE_TYPE == 'Step':
        for y_val in y[0:par.FUN_MIN]:
            assert y_val == 0.0
        for y_val in y[par.FUN_MAX:]:
            assert y_val == par.FUN_PEAK_TO_PEAK
        assert abs( y[int((par.FUN_MIN + par.FUN_MAX) / 2)] ) < par.FUN_PEAK_TO_PEAK * 0.55 and abs( y[round((par.FUN_MIN + par.FUN_MAX) / 2, 0)] ) > par.FUN_PEAK_TO_PEAK * 0.45
    elif par.INITIAL_SURFACE_TYPE == 'V-Shape':
        for y_val in y[0:par.FUN_MIN]:
            assert y_val == 0.0
        for y_val in y[par.FUN_MAX:]:
            assert y_val == 0.0
        assert abs( y[int((par.FUN_MIN + par.FUN_MAX) / 2)] ) > abs( 0.95 * par.FUN_PEAK_TO_PEAK)
    
    #ws.writesurface(x,y)

    plt.title('Surface')
    plt.plot(x, y, 'r+-', label='f(x)')
    plt.legend()
    plt.show()

if __name__ == '__main__':
    test_surfaces()