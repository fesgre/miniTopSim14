"""
    Calculate beam density.
    
    Author: Valentin Kleibel
            Florian Patocka
    (Please add your name if you modify this file)
    """

import numpy as np
import IO.parameters as par
from beam.beam import Beam
from physics import sputtering


def sputFlux(surface):
    """Beam flux from beam current density."""
    return(Beam.beam_flux(surface.vertices[0])*sputtering.syield(surface.angles)*np.abs(np.cos(surface.angles)))

def redepFlux(surface, sputFlux):
    """Particle flux due to redeposition"""
    Fredep = np.zeros_like(sputFlux)
    x, y = surface.vertices
    
    if par.REDEP_1 == False:
        return Fredep
    
    #Calculate the redeposition Flux for all points
    for i in range(len(x)):
        
                    
        #Loop over all neighbors of point i           
        for j in range(len(x)):
            if(i == j):
                continue
            else:
                
                dx, dy = x[i] - x[j], y[i] - y[j]
                dij = np.sqrt(dx**2 + dy**2)
                dx /= dij 
                dy /= dij
                cosAlpha = dx * -surface.directions[0,j] + dy * -surface.directions[1,j]
                cosBeta = -dx * -surface.directions[0,i] - dy * -surface.directions[1,i]
                
         
                if j == 0:
                    deltaL = np.sqrt((x[j]-x[j+1])**2 + (y[j]-y[j+1])**2)
                elif j == (len(x)-1):
                    deltaL = np.sqrt((x[j]-x[j-1])**2 + (y[j]-y[j-1])**2)
                else:
                    deltaL = (np.sqrt((x[j]-x[j+1])**2 + (y[j]-y[j+1])**2)+\
                    np.sqrt((x[j]-x[j-1])**2 + (y[j]-y[j-1])**2))/2
                
                if (cosAlpha < 0 or cosBeta < 0):
                    Fredep[i] += 0
                else:
                    Fredep[i] += 0.5*sputFlux[j]*cosAlpha*cosBeta*deltaL/dij
                    
              
     
  
    return Fredep
   
  
