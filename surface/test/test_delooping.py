# -*- coding: utf-8 -*-
"""
Created on Tue Nov 25 18:45:31 2014

@author: florian
"""

import os, sys
sys.path.insert(0, os.getcwd())

import numpy as np

import IO.parameters as par

from surface.delooping import have_instersection

def test_intersection():
    
    dir_path = os.path.dirname(os.path.abspath(__file__))
    
    par.read(os.path.join(dir_path, 'test_delooping_wodc.cfg'))
    
    print("Doublecheck: " + str(par.LOOP_DOUBLECHECK)) 
    
    seg1_p1 = np.array([1, 1])
    seg1_p2 = np.array([2, 2])

    seg2_p1 = np.array([1, 2])
    seg2_p2 = np.array([2, 1])
    
    passed = True
    
    points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
    
    result, iseg_p = have_instersection(points)
    if result is True:
        print("Intersection: First test passed")
    else:
        print("Intersection: First test not passed")
        passed = False
        
    seg2_p1 = np.array([4, 4])
    seg2_p2 = np.array([3, 3])
    
    points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
    
    result, iseg_p = have_instersection(points)
    if result is False:
        print("Intersection: Second test passed")
    else:
        print("Intersection: Second test not passed")
        passed = False
    
    seg1_p2 = np.array([2, 2, 3])    
    
    try:
        points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
        result, iseg_p = have_instersection(points)
        print("Intersection: Third test not passed")
        passed = False
    except ValueError:
        print("Intersection: Third test passed")

    seg1_p1 = np.array([1, 0])
    seg1_p2 = np.array([2, 1])

    seg2_p1 = np.array([0, 2])
    seg2_p2 = np.array([1, 1])
    
    points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
    
    result, iseg_p = have_instersection(points)
    if result is False:
        print("Intersection: Forth test passed")
    else:
        print("Intersection: Forth test not passed")
        passed = False
    
    par.read(os.path.join(dir_path, 'test_delooping_wdc.cfg'))
    
    print("Doublecheck: " + str(par.LOOP_DOUBLECHECK))
    points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
    result, iseg_p = have_instersection(points)
    if result is False:
        print("Intersection: Fifth test passed")
    else:
        print("Intersection: Fifth test not passed")
        passed = False
    
    seg1_p2 = np.array("string")

    try:
        points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
        result, iseg_p = have_instersection(points)
        print("Intersection: Sixth test not passed")
        passed = False
    except TypeError:
        print("Intersection: Sixth test passed")
        
    seg1_p1 = np.array([1, 1])
    seg1_p2 = np.array([2, 2])

    seg2_p1 = np.array([1, 2])
    seg2_p2 = np.array([2, 1])
    
    points = [seg1_p1, seg1_p2, seg2_p1, seg2_p2]
    
    result, iseg_p = have_instersection(points)
    if result is True:
        print("Intersection: Seventh test passed")
    else:
        print("Intersection: Seventh test not passed")
        passed = False
    
    if passed is True:
        print("Intersection: All tests passed")
    else:
        print("Intersection: Not all tests passed")

if __name__ == '__main__':
    test_intersection()