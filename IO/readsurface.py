"""
Define function to write a surface to a file.

Author: Stefan Durstberger
        Daniel A. Maierhofer
        (Please add your name if you modify this file)
"""

import os
import IO.parameters as par

def readsurface( index ):
    """read a surface from a file and return 2 arrays for x and y values"""
    dir_path = os.path.dirname(os.path.abspath(__file__))
    filename = (os.path.join(dir_path, par.INITIAL_SURFACE_FILE))
    
    x = []
    y = []
    i = 0
    
    f = open(filename)
    for line in f:
        if line.find('surface:') != -1:
            i+=1
    f.seek(0,0)
    
    if index == None:
        index = i
    elif index < 0:
        if abs(index) > i:
            return None, None
        index = i + index + 1
    elif index > i:
        return None, None
    else:
        index += 1
    
    i = 0
    for line in f:
        if line.find('surface:') != -1:
            i+=1
        if index == i:
            values = line.split('\t')         
            try:
                x.append(float(values[0]))
                y.append(float(values[1]))
            except ValueError:
                pass
        if i > index:
            break
    f.close()
    return x,y