"""
    Simulates Backscattering using the Skewed Cosine Function
    
    Author: Gerhard Hobler
    Benedikt Limbacher
    (Please add your name if you modify this file)
    """

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate, interpolate
from math import pi
import os

class SkewedCosine:
    
    def Skew(self, x,x0):
        """Skew Function"""
        return (x-x0)/(1-x*x0)

    def __PartSkewedCosine_1(self, alpha, alpha0, theta,n):
        """Non-scaled Skewed Cosine Function. Do not use!"""
        S=self.Skew(2*alpha/pi, 2*alpha0/pi)
        cvals=np.cos(pi/2*S)**n

        return cvals

    def SkewedCosine_1(self, alpha, alpha0, theta,n, debug=False):
        """Skewed Cosine Function"""
        f=lambda alpha, alpha0, theta,n: self.__PartSkewedCosine_1(alpha,alpha0,theta,n)
        A, err=integrate.quad(f,-pi/2,pi/2, args=(alpha0, theta, n))
        if(debug):
            print("A:"+str(A))
        return 1/A*self.__PartSkewedCosine_1(alpha, alpha0, theta,n)

    def PlotSkewedCosine_1(self, alpha0, theta, n, stepsize, show):
        """Plots Skewed Cosine Function in [-pi/2,pi/2]."""
        alpha=np.arange(-pi/2,pi/2,stepsize)
        f=self.SkewedCosine_1(alpha,alpha0,theta,n)

        plt.title("Skewed Cosine Function")
        plt.plot(alpha*180/pi, f,
                  label=("("+str(self.__ToDegrees(theta))+
                         "deg.,"+str(self.__ToDegrees(alpha0))+
                         " deg.,"+str(n)+")"))
        plt.xlabel(r"$\alpha$ in degrees", fontsize=16)
        plt.ylabel(r"$f_{back}(\alpha)$", fontsize=16)
        
        if(show):
            plt.legend(loc='upper left')
            plt.grid()
            plt.show()

    def __ToRadians(self, angle):
        """Converts degrees to radians"""
        return angle*pi/180
    def __ToDegrees(self, angle):
        """Converts radians to degrees"""
        return angle*180/pi

    def __Task1Plot(self):
        thetas=[0,30,60,70,80,85]
        alpha0s=[0,42.92,63.55,72.43,80.86,85.06]
        ns=[1.78,2.74,2.49,2.36,2.30,2.29]
        l=len(thetas)
        for i in range(0,l):
            b=False
            if(i==l-1):
                b=True
            self.PlotSkewedCosine_1(self.__ToRadians(alpha0s[i]),
                             self.__ToRadians(thetas[i]),
                             ns[i],0.01, b)

    def __ReadDataFromFile(self, file=None):
        """Reads backscattering angular distribution from file. Returns alpha0, power."""
        _file=os.path.join(os.path.dirname(__file__),"..","tables",
                   "gasi30","bang_scp.pp")

        if(file==None):
            data=np.loadtxt(str(_file))
        else:
            data=np.loadtxt(file)

        linesw0=np.where(data==0.0)

        for i in linesw0[0]:
            if(i>0):
                index=i
                break
        
        alpha0=data[0:index+1]
        power=data[index+1:len(data)]

        return alpha0, power

    def __GetPolynomial(self, data, isDeg=False):
        x=data[:,0]*pi/180
        y=data[:,[1,2]]

        if(isDeg):
            y[:,0]*=pi/180
        else:
            y[:,1]*=180/pi #Ableitung in Radianten

        spline=interpolate.PiecewisePolynomial(x,y)
        return spline

    def __PlotPolynomial(self, spline, start, end, stepwidth, label, factor=1):
        x=np.arange(start, end, stepwidth)

        #plt.title(label)
        plt.plot(x*180/pi, spline(x)*factor)
        plt.xlabel(r"$\theta$ in degrees", fontsize=16)
        plt.ylabel(label, fontsize=16)
        
        plt.grid()
        plt.show()

    def __PlotPolynomialD(self, spline, start, end, stepwidth, label, factor=1):
        x=np.arange(start, end, stepwidth)

        #plt.title(label)
        plt.plot(x*180/pi, spline.derivative(x)*factor)
        plt.xlabel(r"$\theta$ in degrees", fontsize=16)
        plt.ylabel(label, fontsize=16)
        
        plt.grid()
        plt.show()

    def __combine(self, x1, x2): #Zum Berechnen der Vereinigungsmenge der Stuetzen
        x=np.unique(np.append(x1[:,0],x2[:,0]))
        return x

    def __PartSkewedCosine_1_da0(self, alpha,alpha0,theta,n):
        return ((pi**2*n*(4*alpha**2-pi**2)*
                    np.sin((pi**2*(alpha-alpha0))/(4*alpha*alpha0-pi**2))*
                    np.cos(pi**2*(alpha-alpha0)/(4*alpha*alpha0-pi**2))**(n-1))/
                    (4*alpha*alpha0-pi**2)**2)

    def __PartSkewedCosine_1_dn(self, alpha,alpha0,theta,n):
        _cv=(np.cos(pi**2*(alpha-alpha0)/(4*alpha*alpha0-pi**2)))
        if(_cv>0):
            return (_cv**n)*np.log(_cv)
        else:
            return 0

    def __GetPolynomialA(self, alpha0, n):
        """Creates the interpolation for A"""
        np.seterr(invalid='ignore')
        
        Ax=self.__combine(alpha0, n)*pi/180
        sa0=self.__GetPolynomial(alpha0, True) #splines
        sn=self.__GetPolynomial(n)

        #print("10\t"+str(sa0(10*pi/180)*180/pi)+"\t"+str(sa0.derivative(10*pi/180)))
        #print("67\t"+str(sn(67*pi/180))+"\t"+str(sn.derivative(67*pi/180)*pi/180))

        f=lambda alpha, alpha0, theta,n: self.__PartSkewedCosine_1(alpha,alpha0,theta,n)
        g=lambda alpha, alpha0, theta,n: self.__PartSkewedCosine_1_da0(alpha,alpha0,theta,n)
        h=lambda alpha, alpha0, theta,n: self.__PartSkewedCosine_1_dn(alpha,alpha0,theta,n)

        A=np.array([])
        dA=np.array([])
        output=np.array([[],[]])
        
        for theta in Ax:
            csa0=sa0(theta)
            csn=sn(theta)

            _A, err=integrate.quad(f,-pi/2,pi/2,
                                   args=(csa0,
                                         theta, csn))
            A=np.append(A,_A)

            """calculating 1st derivate"""
            da0=sa0.derivative(theta)#*pi/180
            dn=sn.derivative(theta)

            """dAda0=((pi**2*csn*(4*alpha**2-pi**2)*
                    np.sin((pi**2*(alpha-csa0))/(4*alpha*csa0-pi**2))*
                    np.cos(pi**2*(alpha-csa0)/(4*alpha*csa0-pi**2))**(csn-1))/
                    (4*alpha*csa0-pi**2)**2)

            dAda0=__PartSkewedCosine_1_da0(alpha, csa0, theta, csn)"""

            dAda0, err=integrate.quad(g,-pi/2,pi/2,
                                      args=(csa0,
                                            theta, csn))
            
            """_cv=(np.cos(pi**2*(alpha-csa0)/(4*alpha*csa0-pi**2)))
            if(_cv>0):
                dAdn=(_cv**csn)*np.log(_cv)
            else:
                dAdn=0"""
            dAdn, err=integrate.quad(h,-pi/2,pi/2,
                                     args=(csa0,theta,csn))

            _dA=dAda0*da0+dAdn*dn


            #print(str(theta*180/pi)+"\t"+str(dAda0)+"\t"+str(dAdn)+"\t"+str(_dA))

            dA=np.append(dA, _dA)
        output=np.column_stack((Ax*180/pi,A, dA*pi/180))
        spline=self.__GetPolynomial(output)
        return spline, sa0, sn

    def SkewedCosine(self, alpha,theta):
        """Skewed Cosine Function"""
        csa0=self.gsa0(theta)
        csn=self.gsn(theta)
        csA=self.gsA(theta)

        f=1/csA*np.cos(pi/2*self.Skew(alpha/(pi/2), csa0/(pi/2)))**(csn)

        return f

    def PlotSkewedCosine(self, theta, stepsize=.1, show=True):
        """Plots Skewed Cosine Function in [-pi/2,pi/2]."""
        alpha=np.arange(-pi/2,pi/2,stepsize)
        #f=np.array([])
        f=self.SkewedCosine(alpha, theta)

        """for a in alpha:
            f=np.append(f, self.SkewedCosine(a, theta))"""

        plt.title("Skewed Cosine Function")
        plt.plot(alpha*180/pi, f,
                  label=("("+str(self.__ToDegrees(theta))+"deg.)"))
        plt.xlabel(r"$\alpha$ in degrees", fontsize=16)
        plt.ylabel(r"$f_{back}(\alpha)$", fontsize=16)
        
        if(show):
            plt.legend(loc='upper left')
            plt.grid()
            plt.show()

    def __init__(self, file=None):
        a0, n=self.__ReadDataFromFile(file)
        self.gsA, self.gsa0, self.gsn=self.__GetPolynomialA(a0, n)

            
    def __Task2Plot(self):
        self.PlotSkewedCosine(0, show=False)
        self.PlotSkewedCosine(self.__ToRadians(30), show=False)
        self.PlotSkewedCosine(self.__ToRadians(60), show=False)
        self.PlotSkewedCosine(self.__ToRadians(70), show=False)
        self.PlotSkewedCosine(self.__ToRadians(80), show=False)
        self.PlotSkewedCosine(self.__ToRadians(85))
        
    def SkewedCosineRunTasks(self):
        """Runs tasks for illustration."""
        self.__PlotPolynomial(self.gsa0, 0, pi/2, 0.0001, r"$\alpha_0(\theta)$", 180/pi)
        #self.__PlotPolynomialD(spline1, 0, pi/2, 0.0001, r"$d\alpha_0(\theta)$")
        self.__PlotPolynomial(self.gsn, 0, pi/2, 0.0001, r"$n(\theta)$")
        #self.__PlotPolynomialD(spline, 0, pi/2, 0.0001, r"$dn(\theta)$", pi/180)
        self.__PlotPolynomial(self.gsA, 0, pi/2, 0.0001, r"$A(\theta)$")
        #self.__PlotPolynomialD(splineA, 0, pi/2, 0.0001, r"$dA(\theta)$")
        self.__Task1Plot()
        self.__Task2Plot()
